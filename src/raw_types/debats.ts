/* tslint:disable */


/**
 * AUTO-GENERATED FILE - DO NOT EDIT!
 *
 * This file was automatically generated by schemats v.3.0.3
 * $ schemats generate -c postgresql://username:password@localhost:5432/debats -t intpjl -t secdis -t typsec -t syndeb -t secdivers -t debats -t lecassdeb -t intdivers -s public
 *
 */


export namespace intpjlFields {
    export type intpjlcle = number;
    export type autcod = string;
    export type secdiscle = number;
    export type intana = string | null;
    export type intfon = string | null;
    export type inturl = string | null;
    export type intordid = number | null;

}

export interface intpjl {
    intpjlcle: intpjlFields.intpjlcle;
    autcod: intpjlFields.autcod;
    secdiscle: intpjlFields.secdiscle;
    intana: intpjlFields.intana;
    intfon: intpjlFields.intfon;
    inturl: intpjlFields.inturl;
    intordid: intpjlFields.intordid;

}

export namespace secdisFields {
    export type secdiscle = number;
    export type lecassidt = string;
    export type typseccod = string;
    export type datsea = Date;
    export type secdisnum = string | null;
    export type secdisobj = string | null;
    export type secdisurl = string | null;
    export type secdisordid = number | null;
    export type secdispere = number | null;

}

export interface secdis {
    secdiscle: secdisFields.secdiscle;
    lecassidt: secdisFields.lecassidt;
    typseccod: secdisFields.typseccod;
    datsea: secdisFields.datsea;
    secdisnum: secdisFields.secdisnum;
    secdisobj: secdisFields.secdisobj;
    secdisurl: secdisFields.secdisurl;
    secdisordid: secdisFields.secdisordid;
    secdispere: secdisFields.secdispere;

}

export namespace typsecFields {
    export type typseccod = string;
    export type typseclib = string;
    export type typseccat = string | null;

}

export interface typsec {
    typseccod: typsecFields.typseccod;
    typseclib: typsecFields.typseclib;
    typseccat: typsecFields.typseccat;

}

export namespace syndebFields {
    export type debsyn = string;
    export type syndeblib = string;

}

export interface syndeb {
    debsyn: syndebFields.debsyn;
    syndeblib: syndebFields.syndeblib;

}

export namespace secdiversFields {
    export type secdiverscle = number;
    export type typseccod = string;
    export type datsea = Date;
    export type secdiverslibelle = string | null;
    export type secdiversobj = string | null;

}

export interface secdivers {
    secdiverscle: secdiversFields.secdiverscle;
    typseccod: secdiversFields.typseccod;
    datsea: secdiversFields.datsea;
    secdiverslibelle: secdiversFields.secdiverslibelle;
    secdiversobj: secdiversFields.secdiversobj;

}

export namespace debatsFields {
    export type datsea = Date;
    export type debsyn = string | null;
    export type autinc = string | null;
    export type deburl = string | null;
    export type numero = number | null;
    export type estcongres = string | null;
    export type libspec = string | null;
    export type etavidcod = string | null;

}

export interface debats {
    datsea: debatsFields.datsea;
    debsyn: debatsFields.debsyn;
    autinc: debatsFields.autinc;
    deburl: debatsFields.deburl;
    numero: debatsFields.numero;
    estcongres: debatsFields.estcongres;
    libspec: debatsFields.libspec;
    etavidcod: debatsFields.etavidcod;

}

export namespace lecassdebFields {
    export type lecassidt = string;
    export type datsea = Date;

}

export interface lecassdeb {
    lecassidt: lecassdebFields.lecassidt;
    datsea: lecassdebFields.datsea;

}

export namespace intdiversFields {
    export type intdiverscle = number;
    export type autcod = string;
    export type secdiverscle = number;
    export type intana = string | null;
    export type intfon = string | null;
    export type intdiversordid = number | null;
    export type inturl = string | null;

}

export interface intdivers {
    intdiverscle: intdiversFields.intdiverscle;
    autcod: intdiversFields.autcod;
    secdiverscle: intdiversFields.secdiverscle;
    intana: intdiversFields.intana;
    intfon: intdiversFields.intfon;
    intdiversordid: intdiversFields.intdiversordid;
    inturl: intdiversFields.inturl;

}
