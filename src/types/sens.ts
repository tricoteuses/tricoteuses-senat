import { sen as Sen } from "../raw_types/sens"

export { Sen }

export interface Photo {
  chemin: string
  cheminMosaique: string
  hauteur: number
  largeur: number
  xMosaique: number
  yMosaique: number
}
