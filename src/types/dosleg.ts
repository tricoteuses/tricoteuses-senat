import { txt_ameliFields } from "../raw_types/ameli"
import {
  ass as Ass,
  aud,
  audFields,
  auteur,
  date_seance,
  date_seanceFields as dateSeanceFields,
  deccoc as DecCoc,
  denrap as DenRap,
  docatt,
  docattFields,
  ecr,
  ecrFields,
  etaloi as EtaLoi,
  lecass,
  lecassFields,
  lecassrap,
  lecture,
  lectureFields,
  loi,
  org as Org,
  oritxt as OriTxt,
  qua as Qua,
  rap,
  raporg as RapOrg,
  raporgFields,
  scr as Scr,
  texte,
  texteFields,
  typatt as TypAtt,
  typlec as TypLec,
  typloi as TypLoi,
  typtxt as TypTxt,
  typurl as TypUrl,
  typurlFields,
} from "../raw_types/dosleg"
import { Debat } from "./debats"
import { TxtAmeli } from "./ameli"

export {
  Ass,
  DecCoc,
  DenRap,
  EtaLoi,
  Org,
  OriTxt,
  Qua,
  RapOrg,
  Scr,
  TypAtt,
  TypLec,
  TypLoi,
  TypTxt,
  TypUrl,
}

export interface Aud extends aud {
  org?: Org
}

export interface Auteur extends auteur {
  qua?: Qua
}

export interface DateSeance extends date_seance {
  debat?: Debat
  scrids?: string[]
  scrs?: Scr[]
}

export interface DocAtt extends docatt {
  rap?: Rap
  typatt?: TypAtt
}

export interface Ecr extends ecr {
  aut?: Auteur
}

export interface LecAss extends lecass {
  ass?: Ass
  auds?: Aud[]
  audcles?: audFields.audcle[]
  datesSeances?: DateSeance[]
  datesSeancesCodes?: dateSeanceFields.code[]
  debatdatseas: Date[]
  lecassraps?: LecAssRap[]
  lecassrapids?: string[]
  org?: Org
  texcods: texteFields.texcod[]
  textes?: Texte[]
}

export interface LecAssRap extends lecassrap {
  rap?: Rap
}

export interface Lecture extends lecture {
  typlec?: TypLec
  lecassidts?: lecassFields.lecassidt[]
  lecasss?: LecAss[]
}

export interface Loi extends loi {
  deccoc?: DecCoc
  etaloi?: EtaLoi
  lecidts?: lectureFields.lecidt[]
  lectures?: Lecture[]
  typloi?: TypLoi
}

export interface Rap extends rap {
  denrap?: DenRap
  docattcles?: docattFields.docattcle[]
  docatts?: DocAtt[]
  ecrnums?: ecrFields.ecrnum[]
  ecrs?: Ecr[]
  orgcods?: raporgFields.orgcod[]
  orgs?: Org[]
}

export interface Texte extends texte {
  ecrs?: Ecr[]
  ecrnums?: ecrFields.ecrnum[]
  libtypurl?: typurlFields.libtypurl
  org?: Org
  oritxt: OriTxt
  typtxt?: TypTxt
  txtAmeli?: TxtAmeli
  txtAmeliId: txt_ameliFields.id
}
