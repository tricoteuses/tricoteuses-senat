import { ses as Ses, sub as Sub, subFields, txt_ameli } from "../raw_types/ameli"

export { Ses, Sub }

export interface TxtAmeli extends txt_ameli {
  subids?: subFields.id[]
}
