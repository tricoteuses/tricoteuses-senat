import { TxtAmeli } from "./types/ameli"
import { Debat } from "./types/debats"
import {
  Ass,
  Aud,
  Auteur,
  DateSeance,
  DecCoc,
  DenRap,
  DocAtt,
  Ecr,
  EtaLoi,
  LecAss,
  LecAssRap,
  Lecture,
  Loi,
  Org,
  OriTxt,
  Qua,
  Rap,
  Scr,
  Texte,
  TypAtt,
  TypLec,
  TypLoi,
  TypTxt,
  TypUrl,
} from "./types/dosleg"

export interface OutputData {
  readonly ass?: { [id: string]: Ass }
  readonly aud?: { [id: string]: Aud }
  readonly auteur?: { [id: string]: Auteur }
  readonly date_seance?: { [id: string]: DateSeance }
  readonly debats?: { [id: string]: Debat }
  readonly deccoc?: { [id: string]: DecCoc }
  readonly denrap?: { [id: string]: DenRap }
  readonly docatt?: { [id: string]: DocAtt }
  readonly ecr?: { [id: string]: Ecr }
  readonly etaloi?: { [id: string]: EtaLoi }
  readonly lecass?: { [id: string]: LecAss }
  readonly lecassrap?: { [id: string]: LecAssRap }
  readonly lecture?: { [id: string]: Lecture }
  readonly loi?: { [id: string]: Loi }
  readonly org?: { [id: string]: Org }
  readonly oritxt?: { [id: string]: OriTxt }
  readonly qua?: { [id: string]: Qua }
  readonly rap?: { [id: string]: Rap }
  readonly scr?: { [id: string]: Scr }
  readonly texte?: { [id: string]: Texte }
  readonly typatt?: { [id: string]: TypAtt }
  readonly typlec?: { [id: string]: TypLec }
  readonly typloi?: { [id: string]: TypLoi }
  readonly typtxt?: { [id: string]: TypTxt }
  readonly typurl?: { [id: string]: TypUrl }
  readonly txt_ameli?: { [id: string]: TxtAmeli }
}

type VisitedIdsByTableName = { [tableName: string]: Set<number | string> }

export function insertAudReferences(
  aud: Aud,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.aud
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.aud = new Set()
  }
  if (!visitedIds.has(aud.audcle)) {
    visitedIds.add(aud.audcle)

    if (aud.orgcod !== null && data.org !== undefined) {
      const org = data.org[aud.orgcod]
      if (org !== undefined) {
        aud.org = org
        // insertOrgReferences(org, data, visitedIdsByTableName)
      }
    }

    // TODO
  }

  return aud
}

export function insertAuteurReferences(
  auteur: Auteur,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.auteur
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.auteur = new Set()
  }
  if (!visitedIds.has(auteur.autcod)) {
    visitedIds.add(auteur.autcod)

    if (data.qua !== undefined) {
      const qua = data.qua[auteur.quacod]
      if (qua !== undefined) {
        auteur.qua = qua
        // insertQuaReferences(qua, data, visitedIdsByTableName)
      }
    }

    // TODO
  }

  return auteur
}

export function insertDateSeanceReferences(
  dateSeance: DateSeance,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.date_seance
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.date_seance = new Set()
  }
  if (!visitedIds.has(dateSeance.code)) {
    visitedIds.add(dateSeance.code)

    if (dateSeance.date_s !== null && data.debats !== undefined) {
      const dateString =
        dateSeance.date_s instanceof Date
          ? dateSeance.date_s.toISOString()
          : dateSeance.date_s
      const debat = data.debats[dateString]
      if (debat !== undefined) {
        dateSeance.debat = debat
        // insertDebatReferences(debat, data, visitedIdsByTableName)
      }
    }

    dateSeance.scrs = []
    if (dateSeance.scrids !== undefined && data.scr !== undefined) {
      for (const scrid of dateSeance.scrids) {
        const scr = data.scr[scrid]
        if (scr !== undefined) {
          dateSeance.scrs.push(scr)
          // insertScrReferences(scr, data, visitedIdsByTableName)
        }
      }
    }
  }

  return dateSeance
}

export function insertDocAttReferences(
  docatt: DocAtt,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.docatt
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.docatt = new Set()
  }
  if (!visitedIds.has(docatt.docattcle)) {
    visitedIds.add(docatt.docattcle)

    if (docatt.rapcod !== null && data.rap !== undefined) {
      const rap = data.rap[docatt.rapcod]
      if (rap !== undefined) {
        docatt.rap = rap
        insertRapReferences(rap, data, visitedIdsByTableName)
      }
    }

    if (docatt.typattcod !== null && data.typatt !== undefined) {
      const typatt = data.typatt[docatt.typattcod]
      if (typatt !== undefined) {
        docatt.typatt = typatt
        // insertTypAttReferences(typatt, data, visitedIdsByTableName)
      }
    }
  }

  return docatt
}

export function insertEcrReferences(
  ecr: Ecr,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.ecr
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.ecr = new Set()
  }
  if (!visitedIds.has(ecr.ecrnum)) {
    visitedIds.add(ecr.ecrnum)

    if (ecr.autcod !== null && data.auteur !== undefined) {
      const aut = data.auteur[ecr.autcod]
      if (aut !== undefined) {
        ecr.aut = aut
        insertAuteurReferences(aut, data, visitedIdsByTableName)
      }
    }

    // TODO
  }

  return ecr
}

export function insertLecassrapReferences(
  lecassrap: LecAssRap,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.lecassrap
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.lecassrap = new Set()
  }
  const lecasrapid = `${lecassrap.lecassidt} ${lecassrap.rapcod}`
  if (!visitedIds.has(lecasrapid)) {
    visitedIds.add(lecasrapid)

    if (lecassrap.rapcod !== null && data.rap !== undefined) {
      const rap = data.rap[lecassrap.rapcod]
      if (rap !== undefined) {
        lecassrap.rap = rap
        insertRapReferences(rap, data, visitedIdsByTableName)
      }
    }
  }

  return lecassrap
}

export function insertLecassReferences(
  lecass: LecAss,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.lecass
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.lecass = new Set()
  }
  if (!visitedIds.has(lecass.lecassidt)) {
    visitedIds.add(lecass.lecassidt)

    if (lecass.codass !== null && data.ass !== undefined) {
      const ass = data.ass[lecass.codass]
      if (ass !== undefined) {
        lecass.ass = ass
        // insertAssReferences(ass, data, visitedIdsByTableName)
      }
    }

    if (lecass.orgcod !== null && data.org !== undefined) {
      const org = data.org[lecass.orgcod]
      if (org !== undefined) {
        lecass.org = org
        // insertOrgReferences(org, data, visitedIdsByTableName)
      }
    }

    lecass.auds = []
    if (lecass.audcles !== undefined && data.aud !== undefined) {
      for (const audcle of lecass.audcles) {
        const aud = data.aud[audcle]
        if (aud !== undefined) {
          lecass.auds.push(aud)
          insertAudReferences(aud, data, visitedIdsByTableName)
        }
      }
    }

    lecass.datesSeances = []
    if (lecass.datesSeancesCodes !== undefined && data.date_seance !== undefined) {
      for (const dateSeanceCode of lecass.datesSeancesCodes) {
        const dateSeance = data.date_seance[dateSeanceCode]
        if (dateSeance !== undefined) {
          lecass.datesSeances.push(dateSeance)
          insertDateSeanceReferences(dateSeance, data, visitedIdsByTableName)
        }
      }
    }

    lecass.lecassraps = []
    if (lecass.lecassrapids !== undefined && data.lecassrap !== undefined) {
      for (const lecassrapid of lecass.lecassrapids) {
        const lecassrap = data.lecassrap[lecassrapid]
        if (lecassrap !== undefined) {
          lecass.lecassraps.push(lecassrap)
          insertLecassrapReferences(lecassrap, data, visitedIdsByTableName)
        }
      }
    }

    lecass.textes = []
    if (lecass.texcods !== undefined && data.texte !== undefined) {
      for (const texcod of lecass.texcods) {
        const texte = data.texte[texcod]
        if (texte !== undefined) {
          lecass.textes.push(texte)
          insertTexteReferences(texte, data, visitedIdsByTableName)
        }
      }
    }
  }

  return lecass
}

export function insertLectureReferences(
  lecture: Lecture,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.lecture
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.lecture = new Set()
  }
  if (!visitedIds.has(lecture.lecidt)) {
    visitedIds.add(lecture.lecidt)

    if (lecture.typleccod !== null && data.typlec !== undefined) {
      const typlec = data.typlec[lecture.typleccod]
      if (typlec !== undefined) {
        lecture.typlec = typlec
        // insertTyplecReferences(typlec, data, visitedIdsByTableName)
      }
    }

    lecture.lecasss = []
    if (lecture.lecassidts !== undefined && data.lecass !== undefined) {
      for (const lecassidt of lecture.lecassidts) {
        const lecass = data.lecass[lecassidt]
        if (lecass !== undefined) {
          lecture.lecasss.push(lecass)
          insertLecassReferences(lecass, data, visitedIdsByTableName)
        }
      }
    }
  }

  return lecture
}

export function insertLoiReferences(
  loi: Loi,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.loi
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.loi = new Set()
  }
  if (!visitedIds.has(loi.loicod)) {
    visitedIds.add(loi.loicod)

    if (loi.typloicod !== null && data.typloi !== undefined) {
      const typloi = data.typloi[loi.typloicod]
      if (typloi !== undefined) {
        loi.typloi = typloi
        // insertTyploiReferences(typloi, data, visitedIdsByTableName)
      }
    }

    if (loi.etaloicod !== null && data.etaloi !== undefined) {
      const etaloi = data.etaloi[loi.etaloicod]
      if (etaloi !== undefined) {
        loi.etaloi = etaloi
        // insertEtaloiReferences(etaloi, data, visitedIdsByTableName)
      }
    }

    if (loi.deccoccod !== null && data.deccoc !== undefined) {
      const deccoc = data.deccoc[loi.deccoccod]
      if (deccoc !== undefined) {
        loi.deccoc = deccoc
        // insertDeccocReferences(deccoc, data, visitedIdsByTableName)
      }
    }

    loi.lectures = []
    if (loi.lecidts !== undefined && data.lecture !== undefined) {
      for (const lecidt of loi.lecidts) {
        const lecture = data.lecture[lecidt]
        if (lecture !== undefined) {
          loi.lectures.push(lecture)
          insertLectureReferences(lecture, data, visitedIdsByTableName)
        }
      }
    }
  }

  return loi
}

export function insertRapReferences(
  rap: Rap,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.rap
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.rap = new Set()
  }
  if (!visitedIds.has(rap.rapcod)) {
    visitedIds.add(rap.rapcod)

    if (rap.coddenrap !== null && data.denrap !== undefined) {
      const denrap = data.denrap[rap.coddenrap]
      if (denrap !== undefined) {
        rap.denrap = denrap
        // insertDenrapReferences(denrap, data, visitedIdsByTableName)
      }
    }

    rap.docatts = []
    if (rap.docattcles !== undefined && data.docatt !== undefined) {
      for (const docattcle of rap.docattcles) {
        const docatt = data.docatt[docattcle]
        if (docatt !== undefined) {
          rap.docatts.push(docatt)
          insertDocAttReferences(docatt, data, visitedIdsByTableName)
        }
      }
    }

    rap.ecrs = []
    if (rap.ecrnums !== undefined && data.ecr !== undefined) {
      for (const ecrnum of rap.ecrnums) {
        const ecr = data.ecr[ecrnum]
        if (ecr !== undefined) {
          rap.ecrs.push(ecr)
          insertEcrReferences(ecr, data, visitedIdsByTableName)
        }
      }
    }

    rap.orgs = []
    if (rap.orgcods !== undefined && data.org !== undefined) {
      for (const orgcod of rap.orgcods) {
        const org = data.org[orgcod]
        if (org !== undefined) {
          rap.orgs.push(org)
          // insertOrgReferences(org, data, visitedIdsByTableName)
        }
      }
    }
  }

  return rap
}

export function insertTexteReferences(
  texte: Texte,
  data: OutputData,
  visitedIdsByTableName: VisitedIdsByTableName,
) {
  let visitedIds = visitedIdsByTableName.texte
  if (visitedIds === undefined) {
    visitedIds = visitedIdsByTableName.texte = new Set()
  }
  if (!visitedIds.has(texte.texcod)) {
    visitedIds.add(texte.texcod)

    texte.ecrs = []
    if (texte.ecrnums !== undefined && data.ecr !== undefined) {
      for (const ecrnum of texte.ecrnums) {
        const ecr = data.ecr[ecrnum]
        if (ecr !== undefined) {
          texte.ecrs.push(ecr)
          insertEcrReferences(ecr, data, visitedIdsByTableName)
        }
      }
    }

    if (texte.orgcod !== null && data.org !== undefined) {
      const org = data.org[texte.orgcod]
      if (org !== undefined) {
        texte.org = org
        // insertOrgReferences(org, data, visitedIdsByTableName)
      }
    }

    if (texte.oritxtcod !== null && data.oritxt !== undefined) {
      const oritxt = data.oritxt[texte.oritxtcod]
      if (oritxt !== undefined) {
        texte.oritxt = oritxt
        // insertOritxtReferences(oritxt, data, visitedIdsByTableName)
      }
    }

    if (texte.txtAmeliId !== undefined && data.txt_ameli !== undefined) {
      const txtAmeli = data.txt_ameli[texte.txtAmeliId]
      if (txtAmeli !== undefined) {
        texte.txtAmeli = txtAmeli
      }
    }

    if (texte.typtxtcod !== null && data.typtxt !== undefined) {
      const typtxt = data.typtxt[texte.typtxtcod]
      if (typtxt !== undefined) {
        texte.typtxt = typtxt
        // insertTyptxtReferences(typtxt, data, visitedIdsByTableName)
      }
    }

    if (texte.typurl !== null && data.typurl !== undefined) {
      const typurl = data.typurl[texte.typurl]
      if (typurl !== undefined) {
        texte.libtypurl = typurl.libtypurl
        // insertTypurlReferences(typurl, data, visitedIdsByTableName)
      }
    }

    // TODO
  }

  return texte
}
